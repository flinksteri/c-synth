
#include <stdlib.h>    // gives malloc
#include <math.h>
#include <unistd.h>		// sleep

#include <OpenAL/al.h>
#include <OpenAL/alc.h>

const double my_pi = 3.14159;
const unsigned sample_rate = 44100;
const size_t buf_size = 60 * sample_rate;
float samples[buf_size];
short samplesout[buf_size];

const char positions[]  =  {0,1,4,5,8,9,12,13,14,16,17,18,12,13,14,15,16,17,18,19,20,21,22,23};
const char notes[]      =  {20,20,22,22,27,27,34,34,22,27, 48,36,22,30,32,  48,36,22,30,32,  };
const char sounds[]     =  {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
const char scales[]     =  {0,2,7,9,12,14,19,24};


float clip(float a){
  if(a>1.0){return 1.0;};
  if(a<0.0){return 0.0;};
  return a;
}

float note2freq(char note){
  float f = (float) note;
  float key = (f-69.0)/12.0;
  return 4.4*pow(2.0,key);

}


void synthesizer(position,duration,sound,note){
  float f = note2freq(note)*440.0;
  for(int i=0; i<duration; i++) {
      float e = 1.0-(i/(float) duration);
      float s = (2.f * my_pi * (f+sin(i*0.001)))/sample_rate * i ;
      s = sin(s)*e*e*e;
      samples[i+position] += s * 0.5;
    }
}

void drum(position,duration,sound,note){
  for(int i=0; i<duration; i++) {
      float e = clip(1.0-(i/60000.0));
      float s = cos(e*e*e*e*e*300.0)*e;
      samples[i+position] +=  s * 0.30;
    }
}

void drum2(position,duration,sound,note){
  for(int i=0; i<duration; i++) {
      float e = clip(1.0-(i/60000.0));
      float s = sin(cos(e*e*9999999.0)*1000.0+i)*e*e*e;
      samples[i+position] +=  s * 0.10;
    }
}


void karplustrong(position,duration,sound,note){

  int stringlength = 100;
  float stringbuffer[stringlength];

  // Pluck string
  float pn = 0.0;
  for(int i=0; i<stringlength; i++) {
    float s1 = sin(cos(i*999999.0)*10000.0+i);
    pn = pn*0.2 + s1*0.8;
    stringbuffer[i] =  pn * cos(i);
  }

  // generate samples
  float noteadd =  note2freq(note);
  float notefreq = 0.0;
  int spos  = 0;

  for(int i=0; i<duration; i++) {

     // move the string
      spos++;
      notefreq += noteadd;

      int   px1 = (spos+stringlength)%stringlength;
      int   px2 = (spos+stringlength+1)%stringlength;
      int   px3 = (spos+stringlength+2)%stringlength;
      int   px4 = (spos+stringlength+10)%stringlength;
      int   pr1 = (stringlength-px1)%stringlength;
      int   pr2 = (stringlength-px2)%stringlength;
      int   pr3 = (stringlength-px3)%stringlength;
      int   pr4 = (stringlength-px4)%stringlength;

      // damp strings
      float filter = 0.1;
      stringbuffer[px2] = (stringbuffer[px1]*filter)+(stringbuffer[px2]*(1.0-filter-filter))+(stringbuffer[px3]*filter);
      stringbuffer[pr2] = (stringbuffer[pr1]*filter)+(stringbuffer[pr2]*(1.0-filter-filter))+(stringbuffer[pr3]*filter);


      // add extra damper to string
      //stringbuffer[px2] = stringbuffer[px2] * 0.9 + stringbuffer[px4]*0.1;

      // add noise to body
      //stringbuffer[px2] += sin(cos(i*999999.0)*10000.0+i)*0.1;

      // decay
      //stringbuffer[px2] *= 0.9;


      // mic string
      float p1 = stringbuffer[px2];
      float p2 = stringbuffer[stringlength-px2];
      float s  = p1 - p2;

      // write sample
      int ws1 = ((int)notefreq)%stringlength;
      int ws2 = (ws1+1)%stringlength;
      float f = notefreq - floor(notefreq);
      float smp = stringbuffer[ws1] * (1.0-f) + stringbuffer[ws2%stringlength] * f;
      samples[i+position] += smp *0.5;

  }

}




void echo(int echosize,float vol){
  for(int i=1; i<buf_size-echosize; i++) {
    samples[i+echosize] += (samples[i-1]*0.5+samples[i]*0.5)*vol;
  }
}


short * calculate(){

  int o = 60000;
  for(int p1 = 0;p1<10;p1++){
    for(int p2 = 0;p2<5;p2++){
      karplustrong(positions[p1]*o+p2*2000,60000,0,notes[p1]+scales[p2]);
    }
  }

   echo(1000,0.2);
   echo(25000,0.4);
   echo(40000,0.4);


  for(int p = 0; p<22;p++){
    drum(p*o,60000,0,notes[p]);
    drum2(p*o+o*0.5,60000,0,notes[p]);
    synthesizer(p*o+o*0.5,60000,0,notes[p]);
  };


  // master
  //float a = 0.1;
  //float c = 0.5;
  for(int i=0; i<buf_size; i++) {
    //c = c*(1.0-a) + abs(samples[i]) * a;
    samplesout[i] = 32760 *  samples[i] * 0.5;
  }

  return samplesout;
}
